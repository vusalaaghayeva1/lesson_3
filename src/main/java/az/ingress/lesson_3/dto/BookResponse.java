package az.ingress.lesson_3.dto;

import az.ingress.lesson_3.model.Book;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class BookResponse {
    Integer id;
    String title;
    String author;
    Integer pageCount;

    public BookResponse(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.pageCount = book.getPageCount();
    }
}
