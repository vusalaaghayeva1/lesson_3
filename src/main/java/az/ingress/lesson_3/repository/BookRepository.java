package az.ingress.lesson_3.repository;

import az.ingress.lesson_3.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Integer> {
}
