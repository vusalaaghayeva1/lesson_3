package az.ingress.lesson_3.controller;

import az.ingress.lesson_3.dto.BookRequest;
import az.ingress.lesson_3.service.BookServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.management.DescriptorKey;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookController {
    private final BookServiceImpl bookService;

    @PostMapping
    public void createBook(@RequestBody BookRequest request){
        bookService.createBook(request);
    }

    @GetMapping("/{id}")
    public void getBookById(@PathVariable Integer id){
        bookService.getBookById(id);
    }

    @GetMapping
    public void getAllBooks(){
        bookService.getAllBooks();
    }

    @PutMapping
    public void updateBookById(@PathVariable Integer id, @RequestBody BookRequest request){
        bookService.updateBook(id,request);
    }

    @DeleteMapping
    public void deleteBookById(@PathVariable Integer id){
        bookService.deleteBookById(id);
    }

}
